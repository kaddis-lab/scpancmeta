## scPancMeta

Interactive application accompanying the publication Van Gurp et al. / [pending citation](http://hirnetwork.org). 

This app has version: 0.4.0 (beta)

### Updates
- updated summary figure for landing 
- updated geneset summary plot data

### To-dos
- add citation
- add data repository links

### Sources
- `prep` contains scripts to process the expression and geneset data, import data (into Monetdb or R data objects), and misc benchmark or plotting tests
- Processed data can be found at: 

