Expression of a gene of interest can be viewed for cell types across datasets. Note that:
- the selection only contains genes that are present in at least two datasets with non-zero expression in at least 5 cells 
- to emphasize any cell type differences, color is relative to the max expression of the selected gene in each dataset
- expression is not mapped for multi and non-endocrine type cells, so these will be hidden when a GOI is selected