# Herrera_Gamma_ID
# Gamma cell identity genes in Mus Musculus
Abcc9
Aqp3
Btg2
Calb1
Chrm3
Etv1
Fgfr1
Fxyd2
Gcnt3
Id2
Id4
Inpp5f
Meis2
Pax6
Ppy
Ptp4a3
Pxk
Sertm1
Slc6a4
Slitrk6
Stmn2
Thsd7a
Tph1
