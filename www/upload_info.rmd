Your file should be .csv or .tsv format, with two required columns:
- First column contains HUGO gene symbols for human (GRCh38) OR mouse (GRCm38). Your geneset options will correspond to the indicated species format of your file. 
- Second column contains the rank values, e.g. -log10 of the adjusted P-values or other values from a suitable ranking method. The analysis will use all rank values, thus your upload should already have undergone any desired FDR-based filtering. 
