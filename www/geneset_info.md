#### Human genesets
- `herrera_*_hs` contains cell type gene sets derived from the main publication
- `hallmark_beta_hs` contains 40 genes specifically up-regulated in pancreatic beta cells from [MSigDB](https://www.gsea-msigdb.org/gsea/msigdb/cards/HALLMARK_PANCREAS_BETA_CELLS)
- `reactome_beta_hs` contains 21 beta cell specific genes from [Reactome Pathways](https://reactome.org/)
- `wang_adult_beta_hs` contains 376 signature adult beta genes from Wang, Yue J et al. *Diabetes* vol. 65,10 (2016): 3028-38. doi:10.2337/db16-0405. [TableS2](https://diabetes.diabetesjournals.org/content/diabetes/suppl/2016/06/30/db16-0405.DC1/DB160405SupplementaryData.pdf)

#### Mouse genesets
- `herrera_*_mmu` contains cell type gene sets derived from the main publication, translated by orthology to mouse
- `hallmark_beta_mmu` contains 41 genes from MSigDB
- `reactome_beta_mmu` contains 22 beta cell specific genes from Reactome Pathways

